Flextesa Extras
===============

Additional modules to test applications with Flextesa/Tezos.

Build & Install
---------------

For now `flextesa-extras` is vendored-in various projects (hence the empty
`.opam` file).

Although one can try with an opam-switch that can build `Tezos` (and Tezos
itself at `../tezos`):

    export OPAMSWITCH=tezos-master-407
    eval $(opam env)
    ln -s ../tezos/
    dune utop src/lib


